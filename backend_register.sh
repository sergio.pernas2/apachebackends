#!/bin/bash


line="server $(hostname -I | sed 's/ *$//'):80;"

frontend=10.0.2.100

if ssh educacionit@$frontend "cat ~/backends/infraestructuras.cloud" | grep "$line"; then

	echo "la direccion ya $(hostname -I) ya esta registrada" 

else
	ssh educacionit@frontend "echo \"$line\" >> ~/backends/infraestructuras.cloud"
	ssh educacionit@frontend "sudo systemctl reload nginx"

fi

